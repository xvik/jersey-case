package com.sample;

import io.dropwizard.Application;
import io.dropwizard.Configuration;
import io.dropwizard.setup.Environment;
import org.glassfish.hk2.utilities.binding.AbstractBinder;


/**
 * @author Vyacheslav Rusakov
 * @since 20.04.2019
 */
public class SampleApp extends Application<Configuration> {

    public static void main(String[] args) throws Exception {
        new SampleApp().run("server");
    }

    @Override
    public void run(Configuration configuration, Environment environment) throws Exception {
        environment.jersey().register(new AbstractBinder() {
            @Override
            protected void configure() {
                 // content not important
            }
        });
    }
}
